#!/bin/bash

##########
# Variables
DOCKER_USER=niels
DOCKER_USER_HOME=/home/${DOCKER_USER}/
DOCKER_DATA=${DOCKER_USER_HOME}/docker-data

docker run -d --net=host -p 8200:8200 -v ${DOCKER_DATA}/minidlna_media:/media -e MINIDLNA_MEDIA_DIR=/media -e MINIDLNA_FRIENDLY_NAME=NHMedia -e MINIDLNA_INOTIFY=yes -e MINIDLNA_NOTIFY_INTERVAL=120 --name=minidlna vladgh/minidlna