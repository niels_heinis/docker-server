#!/bin/bash

##########
# Variables 
DOCKER_USER=niels
DOCKER_USER_HOME=/home/${DOCKER_USER}/
DOCKER_DATA=${DOCKER_USER_HOME}/docker-data

docker run -d --restart=always --name nginx -v ${DOCKER_DATA}/nginx/nginx.conf:/etc/nginx/nginx.conf -v ${DOCKER_DATA}/nginx/conf.d:/etc/nginx/conf.d --net=host nginx:latest
