#!/bin/bash

##########
# Variables
DOCKER_USER=niels
DOCKER_USER_HOME=/home/${DOCKER_USER}/
DOCKER_DATA=${DOCKER_USER_HOME}/docker-data

docker run --name centralmysql --restart=always -v ${DOCKER_DATA}/mysql/conf:/etc/mysql/conf.d -v ${DOCKER_DATA}/mysql/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=U@7dt9fu3F -d mysql:latest