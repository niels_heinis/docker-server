#!/bin/bash

##########
# Variables
DOCKER_USER=niels
DOCKER_USER_HOME=/home/${DOCKER_USER}/
DOCKER_DATA=${DOCKER_USER_HOME}/docker-data
HOST_IP=10.10.10.113
HOST_PORT=8001

docker run -d --restart=always --name intranet -p ${HOST_IP}:${HOST_PORT}:80 -v ${DOCKER_DATA}/intranet:/var/www/html --link centralmysql:mysql synctree/mediawiki