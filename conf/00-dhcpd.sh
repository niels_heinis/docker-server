#!/bin/bash

##########
# Variables
DOCKER_USER=niels
DOCKER_USER_HOME=/home/${DOCKER_USER}/
DOCKER_DATA=${DOCKER_USER_HOME}/docker-data

docker run -d --restart=always --name dhcpd --net=host -v ${DOCKER_DATA}/dhcpd:/data networkboot/dhcpd eth0