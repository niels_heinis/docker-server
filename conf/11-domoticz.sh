#!/bin/bash

##########
# Variables
DOCKER_USER=niels
DOCKER_USER_HOME=/home/${DOCKER_USER}/
DOCKER_DATA=${DOCKER_USER_HOME}/docker-data
HOST_IP=10.10.10.1

# Create and run container
docker run -d --restart=always --name domoticz -v ${DOCKER_DATA}/domoticz:/domoticz/backups -p ${HOST_IP}:8088:8080 --device=/dev/ttyUSB0 hihouhou/domoticz

# Copy existing configuration into container
docker cp domoticz.db_20170329 domoticz:/domoticz/domoticz.db

# Restart container to make configuration effective
docker stop domoticz && docke start domoticz