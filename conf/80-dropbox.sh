#!/bin/bash

##########
# Variables
DOCKER_USER=niels
DOCKER_USER_HOME=/home/${DOCKER_USER}/
DOCKER_DATA=${DOCKER_USER_HOME}/docker-data
HOSTIP=10.10.10.113
DBOX_UID=`id -u niels`
DBOX_GID=`id -g niels`

docker run -d --restart=always --name=dropbox --net="host" -v ${DOCKER_USER_HOME}/Dropbox:/dbox/Dropbox -e DBOX_UID=${DBOX_UID} -e DBOX_GID=${DBOX_GID} janeczku/dropbox
sleep 60
NONCE=`docker logs dropbox 2> /dev/null | grep -m 1 "Please visit"`
echo "###################################################################################################################"
echo ${NONCE}
echo "###################################################################################################################"
# Send push notification with activation URL
MSG="${NONCE}"
APIKEY="0b1d0f753cc8013d99a89113ffc162b3cd48121e"
PROWLURL="https://api.prowlapp.com/publicapi/add"
curl --data "apikey=${APIKEY}&application=Dropbox&event=ActivateDropboxContainer&description=${MSG}" ${PROWLURL}

# Documentation
# + --net="host" enables LAN sync
# + UID/GID must be UID/GID from host user
# + Directory is linked in home dir, not docker-data
#
# More info: https://www.digitalocean.com/community/tutorials/how-to-install-dropbox-client-as-a-service-on-centos-7
