#!/bin/bash

##########
# Variables
DOCKER_USER=niels
DOCKER_USER_HOME=/home/${DOCKER_USER}/
DOCKER_DATA=${DOCKER_USER_HOME}/docker-data
HOSTIP=10.10.10.1

# --net=host gebruiken om DNS werkend te krijgen. Dan geen port forward nodig maar default poorten
# web port: 9080
# api port: 5000

docker run -d --restart=always --net=host -v ${DOCKER_DATA}/jenkins:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock -v ${DOCKER_DATA}/NHP_backups:/var/NHP_backups --name jenkins jenkins:latest
