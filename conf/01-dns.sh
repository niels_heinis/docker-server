#!/bin/bash

##########
# Variables
DOCKER_USER=niels
DOCKER_USER_HOME=/home/${DOCKER_USER}/
DOCKER_DATA=${DOCKER_USER_HOME}/docker-data

# Ports:
# 53 tcp/udp = bind dns
# 10000 = webmin

docker run --name dns -d --restart=always --publish 53:53/tcp --publish 53:53/udp --publish 8005:10000/tcp --volume ${DOCKER_DATA}/dns:/data sameersbn/bind
