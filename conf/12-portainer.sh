#!/bin/bash

##########
# Variables
DOCKER_USER=niels
DOCKER_USER_HOME=/home/${DOCKER_USER}/
DOCKER_DATA=${DOCKER_USER_HOME}/docker-data
HOST_IP=10.10.10.1
HOST_PORT=8002

docker run -d --restart=always -p ${HOST_IP}:${HOST_PORT}:9000 -v /var/run/docker.sock:/var/run/docker.sock -v ${DOCKER_DATA}/portainer:/data --name portainer portainer/portainer
