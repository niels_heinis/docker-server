#!/bin/bash

##########
# Developed and tested for CentOS 7
##########

##########
# Variables
ARG1="$1"
DOCKER_GROUP=docker
DOCKER_USER=niels
DOCKER_USER_HOME=/home/${DOCKER_USER}/
DOCKER_DATA=${DOCKER_USER_HOME}/docker-data
CONF_DIR=

##########
# Install Docker CE
function installDocker
{
  # This function follows instllation instructions as described here:
  # https://docs.docker.com/install/linux/docker-ce/centos/#install-docker-ce
  # Make sure any existing old docker installations are uninstalled
  echo "Removing any legacy docker packages"
  sudo yum remove -q -y docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-selinux docker-engine-selinux docker-engine
  # Setup Docker yum repository
  echo "Preparing for docker install"
  sudo yum install -q -y yum-utils device-mapper-persistent-data lvm2
  sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
  # Install Docker CE
  echo "Instal docker CE"
  sudo yum install docker-ce -y
  # Start & test docker daemon
  sudo systemctl start docker
  sudo systemctl enable docker.service
  sudo docker run --rm hello-world
  if [ ! "$?" -eq 0 ];
  then
    echo "Hello world test failed. Aborting setup. Not cleaning up."
    exit 1
  fi
  
  # Set permissions
  grep -q ${DOCKER_GROUP} /etc/group
  if [ ! "$?" -eq 0 ];
  then
    # Group does not exist
    sudo groupadd ${DOCKER_GROUP}
  fi
  # Add user to docker group
  sudo usermod -aG ${DOCKER_GROUP} ${DOCKER_USER}

  # Following line was used with legacy docker; not expected to be required anymore
  #-- sudo chgrp ${DOCKER_GROUP} "/var/run/docker.sock"
  
  # Clean-up any existing docker config for the usr
  sudo rm -rf "${DOCKER_USER_HOME}/.docker"

  # Test if regular user can run docker commands
  sudo su - niels -c "docker run --rm hello-world"
  if [ ! "$?" -eq 0 ];
  then
    echo "Something went wrong. User niels seems unable to run docker commands. Aborting; not cleaning up."
  else
    echo "Docker installation and preparations finished successfully."
  fi
  
  # Ensure docker is started on boot
  sudo systemctl enable docker
}

function setupStorage 
{
  ##########
  # Setup storage location
  if [ ! -e ${DOCKER_DATA} ];
  then
    mkdir ${DOCKER_DATA}
  fi
}

function setupContainers
{
##########
# From here pull images and start containers. 
# Separate config files per container.
PWD=`pwd`
for c in `ls -1 conf/*.sh`;
do
  echo "*** Running $c ***"
  sudo su - niels -c "${PWD}/$c"
  if [ ! "$?" == 0 ];
  then echo "......FAILED"
  else echo "......SUCCESS"
  fi
done
}

function cleanUp
{
  ##########
  # Clean up
  echo "Cleaning up"
  sudo docker rmi hello-world
}

installDocker
setupStorage
# Setup containers unless --nocontainers param supplied
if [ ! ${ARG1} == "--nocontainers" ];
then
  setupContainers
else
  echo "--nocontainers so no containers created."
fi
cleanUp